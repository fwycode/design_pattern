<?php 
/**
 * +----------------------------------------------------------------------+
 * php23种设计模式实现-单例模式
 * Author：微信公众号：yuantanphp
 * 获取更多资源，技术答疑，项目合作请关注微信公众号：yuantanphp
 * +----------------------------------------------------------------------+
 */
/**单例类 */
class MotorSingleton{
    //用于保存实例
    private static $instance;
    //防止外部创建实例
    private function __construct(){}
    //防止复制实例
    private function __clone(){}
    private $motor = [
        "engine"=>"",
        "body"=>"",
        "whell"=>"",
        "bodyColor"=>"blue"
    ];    //公有方法，用于获取实例
    public static function getInstance(){
        //判断实例有无创建，没有的话创建实例并返回，有的话直接返回
        if(!(self::$instance instanceof self)){
            self::$instance = new self();
        }
        return self::$instance;
    }
    /**发动机组装 */
    public function addEngine($engine){
        $this->motor["engine"] = $engine;
    }
    /**车身组装 */
    public function addBody($body){
        $this->motor["body"] = $body;
    }
    /**车轮组装 */
    public function addWhell($whell){
        $this->motor["whell"] = $whell;
    }
    /**获取摩托 */
    public function getMotor(){
        return $this->motor;
    }
}
/**组装工人 */
class WorkersAssemble{
    //组装摩托车
    public function assemble(){
        MotorSingleton::getInstance()->addEngine("发动机已组装");
        MotorSingleton::getInstance()->addBody("车体已组装");
        MotorSingleton::getInstance()->addWhell("车轮已组装");
    }
}
/**质检员类 */
class QualitativeCheckMember{
    //获取摩托车
    function getMotor(){
        return MotorSingleton::getInstance()->getMotor();
    }
}
$workersAssemble = new WorkersAssemble();
$workersAssemble->assemble();
$qualitativeCheckMember = new QualitativeCheckMember();
$motor = $qualitativeCheckMember->getMotor();
var_dump($motor);