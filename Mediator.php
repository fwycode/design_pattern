<?php
/**
 * +----------------------------------------------------------------------+
 * php23种设计模式实现-中介者模式
 * Author：微信公众号：yuantanphp
 * 获取更多资源，技术答疑，项目合作请关注微信公众号：yuantanphp
 * +----------------------------------------------------------------------+
 */
namespace Mediator;

//公司
abstract class Company
{
    protected $_mediator;  //委托中介
    private $_demand;   //需求
    public $companyNumber;

    public function __construct(Mediator $mediator, $demand)
    {
        $this->_mediator = $mediator;
        $this->_demand = $demand;
    }
    //获取需求
    function getDemand()
    {
        return $this->_demand;
    }
    //寻找工人
    abstract function findWorker();
}
//上海分公司
class CompanyShanghaiA extends Company
{
    function __construct(Mediator $mediator,$message)
    {
        $this->companyNumber = "sh001";
        parent::__construct($mediator,$message);
    }
    //找工人-交给中介
    function findWorker()
    {
        $this->_mediator->findWorker($this);
    }
}
//上海分公司B
class CompanyShanghaiB extends Company
{
    function __construct(Mediator $mediator,$message)
    {
        $this->companyNumber = "sh002";
        parent::__construct($mediator,$message);
    }
    //找工人-交给中介
    function findWorker()
    {
        $this->_mediator->findWorker($this);
    }
}
//上海分公司B
class CompanyShanghaiC extends Company
{
    function __construct(Mediator $mediator,$message)
    {
        $this->companyNumber = "sh003";
        parent::__construct($mediator,$message);
    }
    //找工人-交给中介
    function findWorker()
    {
        $this->_mediator->findWorker($this);
    }
}
//抽象中介类
interface Mediator{
    public function findWorker(Company $company);
}
//具体中介类，为家教匹配合适的学生
class MediatorSh implements Mediator{
    //达成协议的公司编号-为在此列的不提供相关服务
    private $companyList = array("sh001","sh002");
    //人力资源开始按需求招工
    public function findWorker(Company $tutor){
        if(!in_array($tutor->companyNumber,$this->companyList)){
            echo "公司({$tutor->companyNumber})未与人力资源公司达成协议 <br />";
        }
        foreach($this->companyList as $key=>$value){
            if($tutor->companyNumber == $value){
                echo "人力资源公司接到编号为".$tutor->companyNumber." 公司的招聘需求".$tutor->getDemand()." 并开始安排招聘工作"."<br />";
            }
        }
    }
}
$mediatorSh = new MediatorSh();
$demand = "急需200名生产线工人，望加急处理。具体要求见附件Pdf";
$companyShanghaiA = new CompanyShanghaiA($mediatorSh,$demand);
$companyShanghaiA->findWorker();
$demand = "急需10名仓库工作人员，望加急处理。具体要求见附件Pdf";
$companyShanghaiB = new CompanyShanghaiB($mediatorSh,$demand);
$companyShanghaiB->findWorker();
$demand = "急需5名搬运工，望加急处理。具体要求见附件Pdf";
$companyShanghaiB = new CompanyShanghaiC($mediatorSh,$demand);
$companyShanghaiB->findWorker();
