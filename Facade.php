<?php
/**
 * +----------------------------------------------------------------------+
 * php23种设计模式实现-门面模式
 * Author：微信公众号：yuantanphp
 * 获取更多资源，技术答疑，项目合作请关注微信公众号：yuantanphp
 * +----------------------------------------------------------------------+
 */
namespace Facade;
//抽象类-定义生产摩托车流水线标准  Target 目标类
interface  MotorcycleProduce 
{
    //发动机方法
    public function addEngine();
    //车身方法
    public function addBody();
    //车轮方法
    public function addWhell();
    //获取摩托
    public function getMotor();
    //自定义部分
    public function addOther(string $other);
}
//摩托车产品本身
class MotocycleProduct{
    private $motor = [
        "engine"=>"",
        "body"=>"",
        "whell"=>"",
        "bodyColor"=>"blue",
        "other"=>""
    ];
    //新增发动机零部件
    public function addEngine($engine){
        $this->motor["engine"] = $engine;
    }
    public function addBody($body){
        $this->motor["body"] = $body;
    }
    public function addWhell($whell){
        $this->motor["whell"] = $whell;
    }
    public function addOther($other){
        $this->motor["other"] = $other;
    }
    //获取完整摩托对象
    public function getMotor(){
        return $this->motor;
    }
}
class MotorcycleScooter implements MotorcycleProduce
{
    private $motor;
    public function __construct()
    {
        $this->motor = new MotocycleProduct();
    }
    function addEngine()
    {
        $this->motor->addEngine("踏板摩托-发动机已装好");
    } 
    function addBody()
    {
        $this->motor->addBody("踏板摩托-车身已装好");
    }
    function addWhell()
    {
        $this->motor->addWhell("踏板摩托-车轮已装好");
    }
    function addOther($other){
        $this->motor->addOther($other);
    }
    function getMotor()
    {
        return $this->motor->getMotor();
    }
}
//安排生产线开始组装摩托
class Director{
    public function assemble(MotorcycleProduce $motorcycleProduce,$other){
        $motorcycleProduce->addEngine();
        $motorcycleProduce->addBody();
        $motorcycleProduce->addWhell();
        $motorcycleProduce->addOther($other);
    }
}
class Order{
    public function run(){
        echo "已下单<br>";
    }
}
class Pay{
    public function run(){
        echo "已支付<br>";
    }
}

class MotorFace{
    protected $order;
    protected $pay;
    protected $motorScooter;
    protected $director;
    public function __construct()
    {
        $this->order = new Order;
        $this->pay = new Pay;
        $this->motorScooter = new MotorcycleScooter;
        $this->director = new Director();
    }
    public function buy($other){
        $this->order->run();
        $this->pay->run();
        $this->director->assemble($this->motorScooter,$other);
    }
    public function getMotor(){
        return $this->motorScooter->getMotor();
    }
}


$motorFace = new MotorFace;
$motorFace->buy("麻烦帮我把排气管改一下，声音特别炸裂的那种，我去炸街，正所谓。鬼火一响，黄金万两");
$motor = $motorFace->getMotor();
var_dump($motor);