## 阅读须知

## 简要

包含23种设计模式php实现实例，分别如下

入口文件：index.php

访问范例：配置的域名?pattern=Prototype

## 文件说明

Singleton - 单例模式

Factory - 工厂模式

AbstractFactory - 抽象工厂模式

BuilderPattern - 建造者模式

Prototype - 原型模式

Adapter - 适配器模式结构型

AdapterObj - 适配器模式对象型

Bridge - 桥接模式

Composite - 合成模式

Decorator - 装饰器模式

Facade - 门面模式

Proxy - 代理模式

Flyweight - 享元模式

Strategy - 策略模式

Template - 模板方式模式

Observer - 观察者模式

Iterator - 迭代器模式

Responsibility - 责任链模式

Command - 命令模式

Memorandum - 备忘录模式

State - 状态模式

Mediator - 中介者模式

Interpreter - 解释器模式

Visitor - 访问者模式







