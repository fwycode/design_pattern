<?php
/**
 * +----------------------------------------------------------------------+
 * php23种设计模式实现-建造者模式
 * Author：微信公众号：yuantanphp
 * 获取更多资源，技术答疑，项目合作请关注微信公众号：yuantanphp
 * +----------------------------------------------------------------------+
 */
//抽象类-定义生产摩托车流水线标准
abstract class MotorcycleProduce
{
    protected $motor;
    //发动机方法
    public abstract function addEngine();
    //车身方法
    public abstract function addBody();
    //车轮方法
    public abstract function addWhell();
    //获取摩托
    public abstract function getMotor();
}
/**
 * 踏板摩托组装
 */
class MotorcycleScooterProduce extends MotorcycleProduce
{
    public function __construct()
    {
        $this->motor = new MotocycleProduct();
    }
    public function addEngine()
    {
        $this->motor->addEngine("踏板摩托-发动机已装好");
    }
    public function addBody(){
        $this->motor->addBody("踏板摩托-车身已装好");
    }
    public function addWhell(){
        $this->motor->addWhell("踏板摩托-车轮已装好");
    }
    public function getMotor(){
        return $this->motor;
    }
}
/**
 * 跨骑摩托组装
 */
class MotorcycleStraddleProduce extends MotorcycleProduce
{
    public function __construct()
    {
        $this->motor = new MotocycleProduct();
    }
    public function addEngine()
    {
        $this->motor->addEngine("跨骑摩托-发动机已装好");
    }
    public function addBody(){
        $this->motor->addBody("跨骑摩托-车身已装好");
    }
    public function addWhell(){
        $this->motor->addWhell("跨骑摩托-车轮已装好");
    }
    public function getMotor(){
        return $this->motor;
    }
}
//安排生产线开始组装摩托
class Director{
    public function assemble(MotorcycleProduce $motorcycleProduce){
        $motorcycleProduce->addEngine();
        $motorcycleProduce->addBody();
        $motorcycleProduce->addWhell();
    }
}
//摩托车产品本身
class MotocycleProduct{
    private $motor = [
        "engine"=>"",
        "body"=>"",
        "whell"=>""
    ];
    //新增发动机零部件
    public function addEngine($engine){
        $this->motor["engine"] = $engine;
    }
    public function addBody($body){
        $this->motor["body"] = $body;
    }
    public function addWhell($whell){
        $this->motor["whell"] = $whell;
    }
    //获取完整摩托对象
    public function getMotor(){
        return $this->motor;
    }
}
//分配任务管理者
$director = new Director();

//踏板摩托车
$motorcycleScooterProduce = new MotorcycleScooterProduce;
$director->assemble($motorcycleScooterProduce);
$motorcycleScooter = $motorcycleScooterProduce->getMotor();
var_dump($motorcycleScooter);
echo "<br>";
//跨骑摩托车
$motorcycleStraddleProduce = new MotorcycleStraddleProduce;
$director->assemble($motorcycleStraddleProduce);
$motorcycleStraddle = $motorcycleStraddleProduce->getMotor();
var_dump($motorcycleStraddle);