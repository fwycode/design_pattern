<?php
/**
 * +----------------------------------------------------------------------+
 * php23种设计模式实现-模板方法模式
 * Author：微信公众号：yuantanphp
 * 获取更多资源，技术答疑，项目合作请关注微信公众号：yuantanphp
 * +----------------------------------------------------------------------+
 */
/**摩托销售模板类 */
abstract class MotorSaleTemplate{
    protected $sale_volu;  //销售额
    protected $hook;        //狗子
    protected $commission;  //提成
    protected $target_sale_volu;    //目标销售额
    //模板方法
    function template($sale_volu)
    {
        $this->sale_volu = $sale_volu;
        $this->hook = $sale_volu >= $this->target_sale_volu;
        $this->setBonus();
        // $this->customer = $customer;
    }
    //计算奖金
    abstract function setBonus();
    abstract function getBonus();
}
class MotorProxyA extends MotorSaleTemplate{
    public function setBonus()
    {
        $this->commission = 0;
        $this->target_sale_volu = 1000000;
        if($this->hook){
            $this->commission = $this->sale_volu * 0.15;
        }
    }
    public function getBonus()
    {
        return $this->commission;
    }
}
class MotorProxyB extends MotorSaleTemplate{
    public function setBonus()
    {
        $this->commission = 0;
        $this->target_sale_volu = 1000000;
        if($this->hook){
            $this->commission = $this->sale_volu * 0.10;
        }
    }
    public function getBonus()
    {
        return $this->commission;
    }
}

$saleVolu = 150000;
$motorProxyA = new MotorProxyA();
$motorProxyA->template($saleVolu);
var_dump($motorProxyA->getBonus());
echo "<br>";
$saleVolu = 150000;
$motorProxyB = new MotorProxyB();
$motorProxyB->template($saleVolu);
var_dump($motorProxyB->getBonus());