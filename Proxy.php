<?php
/*
 * +----------------------------------------------------------------------+
 * php23种设计模式实现-代理模式
 * Author：微信公众号：yuantanphp
 * 获取更多资源，技术答疑，项目合作请关注微信公众号：yuantanphp
 * +----------------------------------------------------------------------+
 */
/**摩托销售公共接口 */
interface  MotorcycleSaleP {
    /**获取摩托 */
    function getMotor();
    /**销售摩托 */
    function saleMotor();
}
/**摩托供应商 */
class MotorSupplier implements MotorcycleSaleP{
    private $salesman;  //业务员
    private $customer;  //顾客
    function __construct($salesman,$customer)
    {
        $this->salesman = $salesman;
        $this->customer = $customer;
    }
    function getMotor(){
        echo "业务员：{$this->salesman}提取摩托一台进行销售<br/>";
    }
    function saleMotor()
    {
        echo "销售对象：{$this->customer}<br/>";
    }
}
class MotorProxy implements MotorcycleSaleP{
    private $motorSupplier;
    function __construct($salesman,$customer)
    {
        $this->motorSupplier=new MotorSupplier($salesman,$customer);
    }
    function getMotor(){
        $this->motorSupplier->getMotor();
    }
    function saleMotor()
    {
        $this->motorSupplier->saleMotor();
    }
}
$motorProxy = new MotorProxy("冯先生","叶先生");
$motorProxy->getMotor();
$motorProxy->saleMotor();
