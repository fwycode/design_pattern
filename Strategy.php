<?php
/**
 * +----------------------------------------------------------------------+
 * php23种设计模式实现-策略模式
 * Author：微信公众号：yuantanphp
 * 获取更多资源，技术答疑，项目合作请关注微信公众号：yuantanphp
 * +----------------------------------------------------------------------+
 */
//抽象类-定义生产摩托车流水线标准
interface MotorcycleProduce
{
    //发动机方法
    public  function addEngine();
    //车身方法
    public  function addBody();
    //车轮方法
    public  function addWhell();
    //获取摩托
    public  function getMotor();
}
//摩托车产品本身
class MotocycleProduct{
    private $motor = [
        "engine"=>"",
        "body"=>"",
        "whell"=>"",
    ];
    //新增发动机零部件
    public function addEngine($engine){
        $this->motor["engine"] = $engine;
    }
    public function addBody($body){
        $this->motor["body"] = $body;
    }
    public function addWhell($whell){
        $this->motor["whell"] = $whell;
    }
    //获取完整摩托对象
    public function getMotor(){
        return $this->motor;
    }
}
/**
 * 踏板摩托组装
 */
class MotorcycleScooterProduce implements MotorcycleProduce
{
    public function __construct()
    {
        $this->motor = new MotocycleProduct();
    }
    public function addEngine()
    {
        $this->motor->addEngine("踏板摩托-发动机已装好");
    }
    public function addBody(){
        $this->motor->addBody("踏板摩托-车身已装好");
    }
    public function addWhell(){
        $this->motor->addWhell("踏板摩托-车轮已装好");
    }
    public function getMotor(){
        return $this->motor->getMotor();
    }
}
/**
 * 跨骑摩托组装
 */
class MotorcycleScooterProduceNew implements MotorcycleProduce
{
    public function __construct()
    {
        $this->motor = new MotocycleProduct();
    }
    public function addEngine()
    {
        $this->motor->addEngine("新工艺-踏板摩托-发动机已装好");
    }
    public function addBody(){
        $this->motor->addBody("新工艺-踏板摩托-车身已装好");
    }
    public function addWhell(){
        $this->motor->addWhell("新工艺-踏板摩托-车轮已装好");
    }
    public function getMotor(){
        return $this->motor->getMotor();
    }
}
class MotorScooter{
    private $strategy;
    function __construct(MotorcycleProduce $s){
        $this->strategy = $s;
    }
    function getMotor(){
        $this->strategy->addEngine();
        $this->strategy->addBody();
        $this->strategy->addWhell();
        return $this->strategy->getMotor();
    }
}
$motorcycleScooterProduce = new MotorcycleScooterProduce();
$motorScooter = new MotorScooter($motorcycleScooterProduce);
$motor = $motorScooter->getMotor();
var_dump($motor);
echo "<br>";
$motorcycleScooterProduceNew = new MotorcycleScooterProduceNew();
$motorScooter = new MotorScooter($motorcycleScooterProduceNew);
$motor = $motorScooter->getMotor();
var_dump($motor);