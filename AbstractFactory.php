<?php
/**
 * +----------------------------------------------------------------------+
 * php23种设计模式实现-抽象工厂模式
 * Author：微信公众号：yuantanphp
 * 获取更多资源，技术答疑，项目合作请关注微信公众号：yuantanphp
 * +----------------------------------------------------------------------+
 */
/**
 * 摩托发动机制造标准
 */
interface MotorcycleEngineNorms{
    public function engine();
}
/**
 * 踏板摩托发动机制造
 */
class MotorcycleScooterEngine implements MotorcycleEngineNorms{
    public function engine(){
        return "MotorcycleScooterEngine";
    }
}
/**
 * 跨骑摩托发动机制造
 */
class MotorcycleStraddleEngine implements MotorcycleEngineNorms{
    public function engine(){
        return "MotorcycleStraddleEngine";
    }
}
/**
 * 摩托车车体制造标准
 */
interface MotorcycleBodyNorms{
    public function body();
}
/**
 * 踏板摩托车车体制造
 */
class MotorcycleScooterBody implements MotorcycleBodyNorms{
    public function body(){
        return "MotorcycleScooterBody";
    }
}
/**
 * 跨骑摩托车车体制造
 */
class MotorcycleStraddleBody implements MotorcycleBodyNorms{
    public function body(){
        return "MotorcycleStraddleBody";
    }
}
/**
 * 摩托车车轮制造标准
 */
interface MotorcycleWhellNorms{
    public function whell();
}
/**
 * 踏板摩托车车轮制造
 */
class MotorcycleScooterWhell implements MotorcycleWhellNorms{
    public function whell(){
        return "MotorcycleScooterWhell";
    }
}
/**
 * 跨骑摩托车车轮制造
 */
class MotorcycleStraddleWhell implements MotorcycleWhellNorms{
    public function whell(){
        return "MotorcycleStraddleWhell";
    }
}
/**
 * 摩托工厂标准
 */
interface MotorcycleFactory{
    public static function getInstance(string $type);
}
/**
 * 踏板摩托工厂
 */
class MotorcycleScooterFactory implements MotorcycleFactory{
    private static $instance;
    public static function getInstance(string $type)
    {
        switch($type){
            case "engine":
                self::$instance = new MotorcycleScooterEngine();
                break;
            case "body":
                self::$instance = new MotorcycleScooterBody();
                break;
            case "whell":
                self::$instance = new MotorcycleScooterWhell();
                break;
            default:
                throw new Exception("不能生产的摩托车零部件");
                break;
        }
        return self::$instance;
    }
}
/**
 * 跨骑摩托工厂
 */
class MotorcycleStraddleFactory implements MotorcycleFactory{
    private static $instance;
    public static function getInstance(string $type)
    {
        switch($type){
            case "engine":
                self::$instance = new MotorcycleStraddleEngine();
                break;
            case "body":
                self::$instance = new MotorcycleStraddleBody();
                break;
            case "whell":
                self::$instance = new MotorcycleStraddleWhell();
                break;
            default:
                throw new Exception("不能生产的摩托车零部件");
                break;
        }
        return self::$instance;
    }
}
//踏板车
$motoCycleScooter = array();
$motoCycleScooter["engine"] = MotorcycleScooterFactory::getInstance("engine")->engine();
$motoCycleScooter["body"] = MotorcycleScooterFactory::getInstance("body")->body();
$motoCycleScooter["whell"] = MotorcycleScooterFactory::getInstance("whell")->whell();
$motoCycleStraddle = array();
$motoCycleStraddle["engine"] = MotorcycleStraddleFactory::getInstance("engine")->engine();
$motoCycleStraddle["body"] = MotorcycleStraddleFactory::getInstance("body")->body();
$motoCycleStraddle["whell"] = MotorcycleStraddleFactory::getInstance("whell")->whell();

echo "踏板摩托车<br>";
var_dump($motoCycleScooter);
echo "<br>";
echo "跨骑摩托车<br>";
var_dump($motoCycleStraddle);