<?php 
/**
 * +----------------------------------------------------------------------+
 * php23种设计模式实现-适配器模式结构型
 * Author：微信公众号：yuantanphp
 * 获取更多资源，技术答疑，项目合作请关注微信公众号：yuantanphp
 * +----------------------------------------------------------------------+
 */
namespace Adapter;
//抽象类-定义生产摩托车流水线标准  Target 目标类
interface  MotorcycleProduce 
{
    //发动机方法
    public function addEngine();
    //车身方法
    public function addBody();
    public function addBodyAndSetColor(string $color);
    //车轮方法
    public function addWhell();
    //喷漆方法
    public function setBodyColor(string $color);
    //获取摩托
    public function getMotor();
}
//摩托车产品本身
class MotocycleProduct{
    private $motor = [
        "engine"=>"",
        "body"=>"",
        "whell"=>"",
        "bodyColor"=>"blue"
    ];
    //新增发动机零部件
    public function addEngine($engine){
        $this->motor["engine"] = $engine;
    }
    public function addBody($body){
        $this->motor["body"] = $body;
    }
    public function addWhell($whell){
        $this->motor["whell"] = $whell;
    }
    public function setBodyColor(string $color){
        $this->motor["bodyColor"] = $color;
    }
    //获取完整摩托对象
    public function getMotor(){
        return $this->motor;
    }
}
/**
 * 踏板摩托组装  Adapter 适配器类
 */
class motorcycleScooter
{
    protected $motor;
    public function __construct()
    {
        $this->motor = new MotocycleProduct();
    }
    public function addEngine()
    {
        $this->motor->addEngine("踏板摩托-发动机已装好");
    }
    public function addBody(){
        $this->motor->addBody("踏板摩托-车身已装好");
    }
    public function addWhell(){
        $this->motor->addWhell("踏板摩托-车轮已装好");
    }
    public function setBodyColor($color){
        $this->motor->setBodyColor($color);
    }
    public function getMotor(){
        return $this->motor;
    }
}
/**
 * motorcycleScooterAdapter Adaptee 适配者类
 */
class motorcycleScooterAdapter extends motorcycleScooter implements MotorcycleProduce{
    public function addBodyAndSetColor($color){
        $this->addBody();
        $this->setBodyColor("green");
    }
}
$motorcycleScooter = new motorcycleScooterAdapter;
$motorcycleScooter->addEngine();
$motorcycleScooter->addBody();
$motorcycleScooter->addWhell();
$motorcycleScooter->setBodyColor("red");
$motor = $motorcycleScooter->getMotor();
var_dump($motor);
echo "<br>";
$motorcycleScooter = new motorcycleScooterAdapter;
$motorcycleScooter->addEngine();
$motorcycleScooter->addBodyAndSetColor("green");
$motorcycleScooter->addWhell();
$motor = $motorcycleScooter->getMotor();
var_dump($motor);
