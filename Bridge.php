<?php 
/**
 * +----------------------------------------------------------------------+
 * php23种设计模式实现-桥接模式
 * Author：微信公众号：yuantanphp
 * 获取更多资源，技术答疑，项目合作请关注微信公众号：yuantanphp
 * +----------------------------------------------------------------------+
 */
namespace Bridge;
//摩托车产品本身
class MotocycleProduct{
    private $motor = [
        "engine"=>"",
        "body"=>"",
        "whell"=>"",
        "bodyColor"=>"blue"
    ];
    //新增发动机零部件
    public function addEngine($engine){
        $this->motor["engine"] = $engine;
    }
    public function addBody($body){
        $this->motor["body"] = $body;
    }
    public function addWhell($whell){
        $this->motor["whell"] = $whell;
    }
    //获取完整摩托对象
    public function getMotor(){
        return $this->motor;
    }
}
//抽象类-定义生产摩托车流水线标准  Target 目标类
interface  MotorcycleProduce 
{
    //发动机方法
    public function addEngine();
    //车身方法
    public function addBody();
    //车轮方法
    public function addWhell();
    //获取摩托
    public function getMotor();
}
class MotorcycleScooter implements MotorcycleProduce
{
    private $motor;
    public function __construct()
    {
        $this->motor = new MotocycleProduct();
    }
    function addEngine()
    {
        $this->motor->addEngine("踏板摩托-发动机已装好");
    }
    function addBody()
    {
        $this->motor->addBody("踏板摩托-车身已装好");
    }
    function addWhell()
    {
        $this->motor->addWhell("踏板摩托-车轮已装好");
    }
    function getMotor()
    {
        return $this->motor;
    }
}
class MotorcycleStraddle implements MotorcycleProduce{
    public function __construct()
    {
        $this->motor = new MotocycleProduct();
    }
    function addEngine()
    {
        $this->motor->addEngine("跨骑摩托-发动机已装好");
    }
    function addBody()
    {
        $this->motor->addBody("跨骑摩托-车身已装好");
    }
    function addWhell()
    {
        $this->motor->addWhell("跨骑摩托-车轮已装好");
    }
    function getMotor()
    {
        return $this->motor;
    }
}
//桥接抽象类
abstract class Abstraction
{
    protected $motorProduce;
    public function SetImplementor(MotorcycleProduce $motorProduce)
    {
        $this->motorProduce = $motorProduce;
    }
    abstract public function addEngine();
    abstract public function addBody();
    abstract public function addWhell();
    abstract public function getMotor();
}
/**桥接类 */
class RefinedAbstraction extends Abstraction
{
    public function addEngine()
    {
        $this->motorProduce->addEngine();
    }
    public function addBody()
    {
        $this->motorProduce->addBody();
    }
    public function addWhell()
    {
        $this->motorProduce->addWhell();
    }
    public function getMotor()
    {
        return $this->motorProduce->getMotor();
    }
}
$motorcycleScooter = new MotorcycleScooter;
$motorcycleStraddle = new MotorcycleStraddle;

$refinedAbstraction = new RefinedAbstraction;
$refinedAbstraction->SetImplementor($motorcycleScooter);
$refinedAbstraction->addBody();
$refinedAbstraction->addEngine();
$refinedAbstraction->addWhell();
var_dump($refinedAbstraction->getMotor());
echo "<br>";
$refinedAbstraction->SetImplementor($motorcycleStraddle);
$refinedAbstraction->addBody();
$refinedAbstraction->addEngine();
$refinedAbstraction->addWhell();
var_dump($refinedAbstraction->getMotor());
