<?php 
/**
 * +----------------------------------------------------------------------+
 * php23种设计模式实现-装饰器模式
 * Author：微信公众号：yuantanphp
 * 获取更多资源，技术答疑，项目合作请关注微信公众号：yuantanphp
 * +----------------------------------------------------------------------+
 */
namespace Decorator;
//抽象类-定义生产摩托车流水线标准  Target 目标类
interface  MotorcycleProduce 
{
    //发动机方法
    public function addEngine();
    //车身方法
    public function addBody();
    //车轮方法
    public function addWhell();
    //获取摩托
    public function getMotor();
}
//摩托车产品本身
class MotocycleProduct{
    private $motor = [
        "engine"=>"",
        "body"=>"",
        "whell"=>"",
        "bodyColor"=>"blue"
    ];
    //新增发动机零部件
    public function addEngine($engine){
        $this->motor["engine"] = $engine;
    }
    public function addBody($body){
        $this->motor["body"] = $body;
    }
    public function addWhell($whell){
        $this->motor["whell"] = $whell;
    }
    //获取完整摩托对象
    public function getMotor(){
        return $this->motor;
    }
}
class MotorcycleScooter implements MotorcycleProduce
{
    private $motor;
    public function __construct()
    {
        $this->motor = new MotocycleProduct();
    }
    function addEngine()
    {
        $this->motor->addEngine("踏板摩托-发动机已装好");
    } 
    function addBody()
    {
        $this->motor->addBody("踏板摩托-车身已装好");
    }
    function addWhell()
    {
        $this->motor->addWhell("踏板摩托-车轮已装好");
    }
    function getMotor()
    {
        return $this->motor->getMotor();
    }
}
/**
 * 抽象摩托车装饰器类
 */
abstract class MotorDecorator implements MotorcycleProduce{
    protected $motor_cycle;
    public function __construct(MotorcycleProduce $motor_cycle){
        $this->motor_cycle = $motor_cycle;
    }
}
/**
 * 装饰器类-A
 */
class MotorDecoratorA extends MotorDecorator{
    public function addEngine(){
        $this->motor_cycle->addEngine();
    }
    public function addBody()
    {
        $this->motor_cycle->addBody();
    }
    public function addWhell()
    {
        $this->motor_cycle->addWhell();
    }
    public function getMotor()
    {
        $motor = $this->motor_cycle->getMotor();
        $motor["other"] = "个性化配件";
        return $motor;
    }
}
/**
 * 装饰器类
 */
class MotorDecoratorB extends MotorDecorator{
    public function checkMoto(){
        echo "摩托合规性检测-合规<br>";
    }
    public function addEngine(){
        $this->motor_cycle->addEngine();
    }
    public function addBody()
    {
        $this->motor_cycle->addBody();
    }
    public function addWhell()
    {
        $this->motor_cycle->addWhell();
    }
    public function getMotor()
    {
        $motor = $this->motor_cycle->getMotor();
        $this->checkMoto();
        return $motor;
    }
}
$motorScooter = new MotorcycleScooter();
$motorDecoratorA = new MotorDecoratorA($motorScooter);
$motorDecoratorA->addEngine();
$motorDecoratorA->addBody();
$motorDecoratorA->addWhell();
$motor = $motorDecoratorA->getMotor();
var_dump($motor);
echo "<br>";
$motorDecoratorB = new MotorDecoratorB($motorScooter);
$motorDecoratorB->addEngine();
$motorDecoratorB->addBody();
$motorDecoratorB->addWhell();
$motor = $motorDecoratorB->getMotor();
var_dump($motor);



