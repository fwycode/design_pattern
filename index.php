<?php
//入口文件
header("Content-type:text/html; charset=utf-8");
$pattern = $_GET["pattern"];
switch ($pattern) {
    case "Singleton":
        echo "以下为单例模式演示结果<br>";
        require 'Singleton.php';
        break;
    case "Factory":
        echo "以下为工厂模式演示结果<br>";
        require 'Factory.php';
        break;
    case "AbstractFactory":
        echo "以下为抽象工厂模式演示结果<br>";
        require "AbstractFactory.php";
        break;
    case "BuilderPattern":
        echo "以下为建造者模式演示结果<br>";
        require "BuilderPattern.php";
        break;
    case "Prototype":
        echo "以下为原型模式演示结果<br>";
        require "Prototype.php";
        break;
    case "Adapter":
        echo "以下为适配器模式结构型演示结果<br>";
        require "Adapter.php";
        break;
    case "AdapterObj":
        echo "以下为适配器模式对象型演示结果<br>";
        require "AdapterObj.php";
        break;
    case "Bridge":
        echo "以下为桥接模式演示结果<br>";
        require "Bridge.php";
        break;
    case "Composite":
        echo "以下为合成模式演示结果<br>";
        require "Composite.php";
        break;
    case "Decorator":
        echo "以下为装饰器模式演示结果<br>";
        require "Decorator.php";
        break;
    case "Facade":
        echo "以下为门面模式演示结果<br>";
        require "Facade.php";
        break;
    case "Proxy":
        echo "以下为代理模式演示结果<br>";
        require "Proxy.php";
        break;
    case "Flyweight":
        echo "以下为享元模式演示结果<br>";
        require "Flyweight.php";
        break;
    case "Strategy":
        echo "以下为策略模式演示结果<br>";
        require "Strategy.php";
        break;
    case "Template":
        echo "以下为模板方法模式演示结果<br>";
        require "Template.php";
        break;
    case "Observer":
        echo "以下为观察者模式演示结果<br>";
        require "Observer.php";
        break;
    case "Iterator":
        echo "以下为迭代器模式演示结果<br>";
        require "Iterator.php";
        break;
    case "Responsibility":
        echo "以下为责任链模式演示结果<br>";
        require "Responsibility.php";
        break;
    case "Command":
        echo "以下为命令模式演示结果<br>";
        require "Command.php";
        break;
    case "Memorandum":
        echo "以下为备忘录模式演示结果<br>";
        require "Memorandum.php";
        break;
    case "State":
        echo "以下为状态模式演示结果<br>";
        require "State.php";
        break;
    case "Mediator":
        echo "以下为中介者模式演示结果<br>";
        require "Mediator.php";
        break;
    case "Interpreter":
        echo "以下为解释器模式演示结果<br>";
        require "Interpreter.php";
        break;
    case "Visitor":
        echo "以下为访问者模式演示结果<br>";
        require "Visitor.php";
        break;
    default:
        echo "未知模式";
        break;
}
