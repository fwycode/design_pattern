<?php
/**
 * +----------------------------------------------------------------------+
 * php23种设计模式实现-责任链模式
 * Author：微信公众号：yuantanphp
 * 获取更多资源，技术答疑，项目合作请关注微信公众号：yuantanphp
 * +----------------------------------------------------------------------+
 */
//领导者类
abstract class Leader{
    private $leader;
    private $leaveDays;
    //设置下一级管理员
    public function setNext(Leader $leader){
        $this->leader = $leader;
    }
    //获取当前管理员
    public function getNext(){
        return $this->leader;
    }
    //处理下级请假请求
    public abstract function handleRequest($leaveDays);
}
//组长类
class GroupLeader extends Leader{
    //一天以内的假期组长具备权限
    public function __construct()
    {
        $this->leaveDays = 1;
    }
    public function handleRequest($leaveDays){
        if($leaveDays > $this->leaveDays){
            echo "请假 ".$this->leaveDays." 天以上需上级部门批准<br>" ;
        }
        else{
            echo "你的请假请求请假 ".$leaveDays." 天被组长批了<br>";
            exit();
        }
    }
}
//部门主管
class Department extends Leader{
    //五天天以内的假期组长具备权限
    public function __construct()
    {
        $this->leaveDays = 5;
    }    
    public function handleRequest($leaveDays){
        if($leaveDays > $this->leaveDays){
            echo "请假 ".$this->leaveDays." 天以上需上级部门批准<br>" ;
        }
        else{
            echo "你的请假请求请假 ".$leaveDays." 天被部门主管批了<br>";
            exit();
        }
    }
}
class factoryDirector extends Leader{
    //五天天以内的假期组长具备权限
    public function __construct()
    {
        $this->leaveDays = 30;
    }    
    public function handleRequest($leaveDays){
        if($leaveDays > $this->leaveDays){
            echo "请假 ".$this->leaveDays." 天以上需上级部门批准<br>" ;
        }
        else{
            echo "你的请假请求请假 ".$leaveDays." 厂长主管批了<br>";
            exit();
        }
    }
}
//请假天数
$leaveDays = 10;
$groupLeader = new GroupLeader();
$department = new Department();
$factoryDirector = new factoryDirector();
$groupLeader->handleRequest($leaveDays);
$groupLeader->setNext($department);
$next = $groupLeader->getNext();
$next->handleRequest($leaveDays);
$groupLeader->setNext($factoryDirector);
$next = $groupLeader->getNext();
$next->handleRequest($leaveDays);