<?php 
/**
 * +----------------------------------------------------------------------+
 * php23种设计模式实现-原型模式
 * Author：微信公众号：yuantanphp
 * 获取更多资源，技术答疑，项目合作请关注微信公众号：yuantanphp
 * +----------------------------------------------------------------------+
 */
namespace MotorcycleProduce;
//抽象类-定义生产摩托车流水线标准
abstract class MotorcycleProduce
{
    protected $motor;
    //发动机方法
    public abstract function addEngine();
    //车身方法
    public abstract function addBody();
    //车轮方法
    public abstract function addWhell();
    //喷漆方法
    public abstract function setBodyColor(string $color);
    //获取摩托
    public abstract function getMotor();
}
//摩托车产品本身
class MotocycleProduct{
    private $motor = [
        "engine"=>"",
        "body"=>"",
        "whell"=>"",
        "bodyColor"=>"blue"
    ];
    //新增发动机零部件
    public function addEngine($engine){
        $this->motor["engine"] = $engine;
    }
    public function addBody($body){
        $this->motor["body"] = $body;
    }
    public function addWhell($whell){
        $this->motor["whell"] = $whell;
    }
    public function setBodyColor(string $color){
        $this->motor["bodyColor"] = $color;
    }
    //获取完整摩托对象
    public function getMotor(){
        return $this->motor;
    }
}
/**
 * 踏板摩托组装
 */
class motorcycleScooter extends MotorcycleProduce
{
    public function __construct()
    {
        $this->motor = new MotocycleProduct();
    }
    public function __clone()
    {
        $this->motor = clone $this->motor;
    }
    public function addEngine()
    {
        $this->motor->addEngine("踏板摩托-发动机已装好");
    }
    public function addBody(){
        $this->motor->addBody("踏板摩托-车身已装好");
    }
    public function addWhell(){
        $this->motor->addWhell("踏板摩托-车轮已装好");
    }
    public function setBodyColor($color){
        $this->motor->setBodyColor($color);
    }
    public function getMotor(){
        return $this->motor;
    }
}
$motorcycleScooter = new MotorcycleScooter;
$motorcycleScooter->addEngine();
$motorcycleScooter->addBody();
$motorcycleScooter->addWhell();
$motor = $motorcycleScooter->getMotor();
var_dump($motor);
echo "<br>";
$motorcycleScooterRed = clone $motorcycleScooter;
$motorcycleScooterRed->setBodyColor("Red");
$motor = $motorcycleScooterRed->getMotor();
var_dump($motor);
